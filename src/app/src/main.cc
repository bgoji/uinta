#include <absl/status/status.h>

#include "absl/log/log.h"
#include "absl/strings/str_format.h"
#include "uinta/desktop_platform.h"
#include "uinta/engine/engine.h"
#include "uinta/file.h"
#include "uinta/gl.h"
#include "uinta/scenes/demo_scene.h"

int main(int argc, const char** argv) {
  uinta::ArgsProcessor args(argc, argv);
  uinta::WindowConfig cfg;
  uinta::DesktopPlatform platform(cfg, &args);
  auto* fileSystem = uinta::FileSystemImpl::Instance();
  auto* gl = uinta::OpenGLApiImpl::Instance();

  if (!platform.status().ok()) {
    LOG(FATAL) << absl::StrFormat("Failed to initialize `DesktopPlatform`: %s",
                                  platform.status().message());
  }

  uinta::Engine engine(&platform, gl, fileSystem);
  if (!engine.status().ok()) {
    LOG(FATAL) << absl::StrFormat("Failed to initialize `Engine`: %s",
                                  engine.status().message());
  }

  engine.addScene<uinta::DemoScene>();
  engine.run();
  if (!engine.status().ok()) {
    LOG(FATAL) << absl::StrFormat(
        "`Engine::run()` %s failure: %s",
        engine.scenes()->empty()
            ? ""
            : absl::StrFormat("with %s", engine.scenes()->front()->name()),
        engine.status().message());
  }

  LOG(INFO) << "Exiting";
  return EXIT_SUCCESS;
}
