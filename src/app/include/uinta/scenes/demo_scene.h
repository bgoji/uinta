#ifndef SRC_APP_INCLUDE_UINTA_SCENES_DEMO_SCENE_H_
#define SRC_APP_INCLUDE_UINTA_SCENES_DEMO_SCENE_H_

#include "uinta/camera/camera.h"
#include "uinta/camera/camera_config.h"
#include "uinta/camera/camera_manager.h"
#include "uinta/debug/debug_scene.h"
#include "uinta/engine/cursor_manager.h"
#include "uinta/engine/engine.h"
#include "uinta/scene/scene.h"
#include "uinta/scenes/demo_palette.h"
#include "uinta/scenes/fbx_viewer_scene.h"
#include "uinta/scenes/terrain_scene.h"
#include "uinta/scenes/text_scene.h"
#include "uinta/scenes/trees_scene.h"
#include "uinta/shaders/primitive.h"

namespace uinta {

class DemoScene : public Scene {
 public:
  explicit DemoScene(Engine* engine,
                     SceneLayer layer = SceneLayer::Simulation) noexcept
      : Scene(engine, "DemoScene", layer), shader_(engine->gl()) {
    auto palette = Palettes::PrimaryPalette();
    engine->gl()->clearColor(palette[0].r, palette[0].g, palette[0].b,
                             palette[0].a);

    auto config = camera_.config();
    config.vertOff = 0.5;
    camera_.config(config);
    camera_.angle(320);
    camera_.dist(20);
    camera_.pitch(15);
    camera_.force();

    auto* cm = addSystem<CameraManager>(&camera_, engine->dispatchers());
    addSystem<PrimitiveShaderManager>(&shader_, cm);

    addComponent<CursorManager>(engine, cm);

    addScene<DebugScene>();
    addScene<FbxViewerScene>();
    addScene<TerrainScene>();
    addScene<TextScene>();
    addScene<TreeScene>();
  }

 private:
  Camera camera_;
  PrimitiveShader shader_;
};

}  // namespace uinta

#endif  // SRC_APP_INCLUDE_UINTA_SCENES_DEMO_SCENE_H_
