#ifndef SRC_APP_INCLUDE_UINTA_SCENES_CUBE_SCENE_H_
#define SRC_APP_INCLUDE_UINTA_SCENES_CUBE_SCENE_H_

#include "uinta/engine/engine.h"
#include "uinta/gl.h"
#include "uinta/mesh.h"
#include "uinta/scene/scene.h"
#include "uinta/scenes/demo_palette.h"
#include "uinta/shader.h"
#include "uinta/shaders/primitive.h"
#include "uinta/vao.h"
#include "uinta/vbo.h"

namespace uinta {

class CubeScene : public Scene {
 public:
  explicit CubeScene(Scene* parent) noexcept
      : Scene(parent, "CubeScene", SceneLayer::Simulation),
        vbo_(GL_ARRAY_BUFFER, 0, engine()->gl()) {
    size_t idxOffset = 0;
    auto mesh = Mesh::Cube(&idxOffset);
    mesh.color(DemoPalette[4]);

    VboGuard vbg(&vbo_);
    vbo_.bufferData(mesh.vertices().data(),
                    mesh.vertices().size() * Vertex::ElementCount * sizeof(f32),
                    GL_STATIC_DRAW);

    VaoGuard vag(&vao_);
    vao_.ebo(mesh.elements());

    indexCount_ = mesh.elements().size();

    auto sm = parent->findSystem<PrimitiveShaderManager>().value_or(nullptr);
    assert(sm && "`PrimitiveShaderManager` not found.");
    shader_ = sm->shader();

    ShaderGuard sg(shader_);
    shader_->linkAttributes(&vao_);
    shader_->view = glm::translate(glm::mat4(1), glm::vec3(0, 0, -3));
  }

  void render(const EngineState&) noexcept override {
    CullFaceGuard cfg;
    DepthTestGuard dtg;
    ShaderGuard shaderGuard(shader_);
    VaoGuard vaoGuard(&vao_);

    engine()->gl()->drawElements(GL_TRIANGLES, indexCount_, GL_UNSIGNED_INT, 0);
  }

 private:
  Vao vao_;
  Vbo vbo_;
  size_t indexCount_;
  PrimitiveShader* shader_;
};

}  // namespace uinta

#endif  // SRC_APP_INCLUDE_UINTA_SCENES_CUBE_SCENE_H_
