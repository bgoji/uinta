#include "uinta/args.h"

#include <gtest/gtest.h>

namespace uinta {

TEST(Args, InitialState) {
  ArgsProcessor ap(0, nullptr);
  ASSERT_TRUE(ArgsProcessor(0, nullptr).isFullscreen());
}

TEST(Args, NoFullscreen) {
  const char* args = "--no-fullscreen";
  ASSERT_FALSE(ArgsProcessor(1, &args).isFullscreen());
}

}  // namespace uinta
