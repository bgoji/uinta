#ifndef SRC_PLATFORM_INCLUDE_UINTA_ARGS_H_
#define SRC_PLATFORM_INCLUDE_UINTA_ARGS_H_

#include <array>
#include <string>

#include "uinta/flags.h"
#include "uinta/types.h"

namespace uinta {

template <size_t N>
using Key = std::array<std::string, N>;

class ArgsProcessor {
 public:
  struct Keys {
    static constexpr Key<2> Help = {"--help", "-h"};
    static constexpr Key<1> NoFullscreen = {"--no-fullscreen"};
  };

  ArgsProcessor(i32, const char**) noexcept;

  bool isFullscreen() const noexcept { return flags_.isFullscreen(); }

  void isFullscreen(bool v) noexcept { flags_.isFullscreen(v); }

 private:
  struct Flags final {
    using value_type = u32;

    FlagsOperations(FullscreenMask);

    bool isFullscreen() const noexcept { return flags_ & FullscreenMask; }
    void isFullscreen(bool v) noexcept {
      flags_ &= ~FullscreenMask;
      if (v) flags_ |= FullscreenMask;
    }

   private:
    static constexpr value_type FullscreenMask = 1 << 0;

    value_type flags_;
  } flags_;
};

}  // namespace uinta

#endif  // SRC_PLATFORM_INCLUDE_UINTA_ARGS_H_
