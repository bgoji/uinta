#include "uinta/args.h"

#include <absl/strings/str_format.h>

#include <algorithm>
#include <iostream>
#include <string>
#include <unordered_map>
#include <utility>
#include <vector>

#include "absl/log/log.h"
#include "absl/strings/str_cat.h"

namespace uinta {

using ArgStore = std::unordered_map<std::string, std::string>;

void expectNoValue(const ArgStore* args, const std::string& key) noexcept {
  const auto it = args->find(key);
  if (it != args->end() && !it->second.empty()) {
    LOG(FATAL) << "Unexpected value for argument '" << key << "'";
  }
}

void handleNoFullscreen(ArgsProcessor* processor,
                        const ArgStore* args) noexcept {
  for (auto key : ArgsProcessor::Keys::NoFullscreen) {
    if (args->contains(key)) {
      expectNoValue(args, key);
      processor->isFullscreen(false);
      break;
    }
  }
}

bool handleHelp(const ArgStore* args) noexcept {
  bool hasHelpArg = false;
  for (auto key : ArgsProcessor::Keys::Help) {
    if (hasHelpArg = args->contains(key); hasHelpArg) {
      break;
    }
  }
  if (!hasHelpArg) return false;
  std::vector<std::span<const std::string>> collections = {
      std::span<const std::string>(ArgsProcessor::Keys::Help),
      std::span<const std::string>(ArgsProcessor::Keys::NoFullscreen),
  };
  size_t colWidth = 0;
  for (const auto& collection : collections) {
    for (const auto& key : collection) {
      colWidth = std::max(colWidth, key.size());
    }
  }
  constexpr size_t padding = 2;
  colWidth += padding;
  std::string output = "Uinta Engine options:";
  for (const auto& collection : collections) {
    output = absl::StrCat(output, "\n\t");
    for (const auto& key : collection) {
      output = absl::StrCat(output, absl::StrFormat("%-*s", colWidth, key));
    }
  }
  std::cout << output << std::endl;
  return true;
}

std::pair<std::string, std::string> kvp(std::string arg) noexcept {
  constexpr const char* defaultValue = "";
  std::pair<std::string, std::string> result(arg, defaultValue);
  if (auto idx = arg.find('='); idx != std::string::npos) {
    result.first = arg.substr(0, idx);
    result.second = arg.substr(idx, arg.size() - idx - 1);
  }
  return result;
}

ArgsProcessor::ArgsProcessor(i32 argc, const char** argv) noexcept {
  ArgStore args;
  for (auto i = 0; i < argc; i++) {
    args.insert(kvp(argv[i]));
  }

  if (handleHelp(&args)) {
    exit(0);
  }

  handleNoFullscreen(this, &args);
}

}  // namespace uinta
