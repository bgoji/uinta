#ifndef SRC_ENGINE_INCLUDE_UINTA_CAMERA_CAMERA_H_
#define SRC_ENGINE_INCLUDE_UINTA_CAMERA_CAMERA_H_

#include "glm/ext/matrix_float4x4.hpp"
#include "uinta/camera/camera_config.h"
#include "uinta/flags.h"
#include "uinta/math/smooth_float.h"
#include "uinta/math/smooth_vec.h"
#include "uinta/types.h"

namespace uinta {

class EngineState;

class Camera {
 public:
  struct Flags final {
    using value_type = u8;

    static constexpr value_type DistLimitMask = 1 << 0;
    static constexpr value_type PitchLimitMask = 1 << 1;
    static constexpr value_type KeyboardEnabledMask = 1 << 2;
    static constexpr value_type MouseEnabledMask = 1 << 3;
    static constexpr value_type AngleLockMask = 1 << 4;
    static constexpr value_type DistLockMask = 1 << 5;
    static constexpr value_type PitchLockMask = 1 << 6;

    FlagsOperations(DefaultFlags);

    bool isAngleLock() const noexcept { return flags_ & AngleLockMask; }
    void isAngleLock(bool v) noexcept {
      flags_ &= ~AngleLockMask;
      if (v) flags_ |= AngleLockMask;
    }

    bool isDistLimit() const noexcept { return flags_ & DistLimitMask; }
    void isDistLimit(bool v) noexcept {
      flags_ &= ~DistLimitMask;
      if (v) flags_ |= DistLimitMask;
    }

    bool isDistLock() const noexcept { return flags_ & DistLockMask; }
    void isDistLock(bool v) noexcept {
      flags_ &= ~DistLockMask;
      if (v) flags_ |= DistLockMask;
    }

    bool isPitchLimit() const noexcept { return flags_ & PitchLimitMask; }
    void isPitchLimit(bool v) noexcept {
      flags_ &= ~PitchLimitMask;
      if (v) flags_ |= PitchLimitMask;
    }

    bool isPitchLock() const noexcept { return flags_ & PitchLockMask; }
    void isPitchLock(bool v) noexcept {
      flags_ &= ~PitchLockMask;
      if (v) flags_ |= PitchLockMask;
    }

    bool isKeyboardEnabled() const noexcept {
      return flags_ & KeyboardEnabledMask;
    }
    void isKeyboardEnabled(bool v) noexcept {
      flags_ &= ~KeyboardEnabledMask;
      if (v) flags_ |= KeyboardEnabledMask;
    }

    bool isMouseEnabled() const noexcept { return flags_ & MouseEnabledMask; }
    void isMouseEnabled(bool v) noexcept {
      flags_ &= ~MouseEnabledMask;
      if (v) flags_ |= MouseEnabledMask;
    }

   private:
    value_type flags_;
    static constexpr value_type DefaultFlags =
        DistLimitMask | PitchLimitMask | KeyboardEnabledMask | MouseEnabledMask;
  };

  explicit Camera(CameraConfig config = {}) noexcept : config_(config) {}

  ~Camera() noexcept = default;
  Camera(const Camera&) noexcept = delete;
  Camera& operator=(const Camera&) noexcept = delete;
  Camera(Camera&& other) noexcept = delete;
  Camera& operator=(Camera&& other) noexcept = delete;

  const SmoothFloat& angle() const noexcept { return angle_; }

  void angle(f32 angle) noexcept { angle_ = angle; }

  void angle(SmoothFloat angle) noexcept { angle_ = angle; }

  void enableKeyboard(bool v = true) noexcept { flags_.isKeyboardEnabled(v); }

  void enableMouse(bool v = true) noexcept { flags_.isMouseEnabled(v); }

  void lockAngle(bool v = true) noexcept { flags_.isAngleLock(v); }

  void lockDist(bool v = true) noexcept { flags_.isDistLock(v); }

  void lockPitch(bool v = true) noexcept { flags_.isPitchLock(v); }

  const CameraConfig& config() const noexcept { return config_; }

  void config(const CameraConfig& config) noexcept { config_ = config; }

  const SmoothFloat& dist() const noexcept { return dist_; }

  void dist(f32 dist) noexcept { dist_ = dist; }

  void dist(SmoothFloat dist) noexcept { dist_ = dist; }

  Flags flags() const noexcept { return flags_; }

  Flags& flags() noexcept { return flags_; }

  void flags(Flags flags) noexcept { flags_ = flags; }

  const SmoothFloat& pitch() const noexcept { return pitch_; }

  void pitch(f32 pitch) noexcept { pitch_ = pitch; }

  void pitch(SmoothFloat pitch) noexcept { pitch_ = pitch; }

  glm::vec3 position() const noexcept { return position_; }

  const SmoothVec3& target() const noexcept { return target_; }

  void target(const SmoothVec3& target) noexcept { target_ = target; }

  void target(const glm::vec3& target) noexcept { target_ = target; }

  f32 translationFactor() const noexcept;

  void update(const EngineState&) noexcept;

  void force() noexcept {
    target_.force();
    angle_.force();
    dist_.force();
    pitch_.force();
  }

  glm::mat4 viewMatrix() const noexcept;

 private:
  friend class CameraManager;

  CameraConfig config_ = {};
  glm::vec3 position_ = {};
  SmoothVec3 target_ = SmoothVec3(8);
  SmoothFloat angle_ = SmoothFloat(8);
  SmoothFloat dist_ = SmoothFloat(8);
  SmoothFloat pitch_ = SmoothFloat(8);
  Flags flags_;

  void updateAngle(time_t, const Input&) noexcept;
  void updateDist(time_t, const Input&) noexcept;
  void updatePitch(time_t, const Input&) noexcept;
  void updateTranslation(time_t, const Input&) noexcept;

  glm::mat4 perspectiveMatrix(f32 aspectRatio) const noexcept;

  glm::mat4 orthographicMatrix(f32 size, f32 aspectRatio) const noexcept;
};

}  // namespace uinta

#endif  // SRC_ENGINE_INCLUDE_UINTA_CAMERA_CAMERA_H_
