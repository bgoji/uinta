#ifndef SRC_ENGINE_INCLUDE_UINTA_CAMERA_CAMERA_MANAGER_H_
#define SRC_ENGINE_INCLUDE_UINTA_CAMERA_CAMERA_MANAGER_H_

#include <utility>

#include "uinta/camera/camera_comparator.h"
#include "uinta/camera/camera_events.h"
#include "uinta/system.h"

namespace uinta {

class Camera;
class EngineDispatchers;

class CameraManager : public System {
 public:
  enum class Projection : u8 {
    Perspective,
    Orthographic,
  };

  explicit CameraManager(Camera*, EngineDispatchers*,
                         Projection = Projection::Perspective,
                         f32 orthoSize = DefaultOrthoSize) noexcept;

  Camera* camera() noexcept { return camera_; }

  const Camera* camera() const noexcept { return camera_; }

  template <CameraEvent E, typename... Args>
  void addListener(Args&&... args) noexcept {
    dispatchers_.template addListener<E>(std::forward<Args>(args)...);
  }

  void onTick(const EngineState& state) noexcept override {
    camera_->update(state);
  }

  void onNewFrame(const EngineState&) noexcept override {
    if (comparator_ == camera_) return;
    comparator_ = *camera_;
    dispatchers_.dispatch<CameraEvent::ViewMatrixUpdated>(
        ViewMatrixUpdateEvent(camera_, camera_->viewMatrix()));
  }

  auto orthoSize() const noexcept { return orthoSize_; }

  void orthoSize(f32 orthoSize) noexcept {
    orthoSize_ = orthoSize;
    if (projection_ == Projection::Orthographic) {
      dispatchers_.dispatch<CameraEvent::ProjectionMatrixUpdated>(
          ProjectionMatrixUpdateEvent(
              camera_, camera_->orthographicMatrix(orthoSize_, aspect_),
              aspect_));
    }
  }

  void projection(Projection projection) noexcept {
    if (projection == projection_) return;
    projection_ = projection;
    auto mat = projection_ == Projection::Perspective
                   ? camera_->perspectiveMatrix(aspect_)
                   : camera_->orthographicMatrix(orthoSize_, aspect_);
    dispatchers_.dispatch<CameraEvent::ProjectionMatrixUpdated>(
        ProjectionMatrixUpdateEvent(camera_, mat, aspect_));
  }

  Projection projection() const noexcept { return projection_; }

 private:
  static constexpr f32 DefaultOrthoSize = 5;

  CameraComparator comparator_;
  CameraDispatchers dispatchers_;
  f32 aspect_ = 1;
  f32 orthoSize_ = DefaultOrthoSize;
  Projection projection_ = Projection::Perspective;

  Camera* camera_;
};

}  // namespace uinta

#endif  // SRC_ENGINE_INCLUDE_UINTA_CAMERA_CAMERA_MANAGER_H_
