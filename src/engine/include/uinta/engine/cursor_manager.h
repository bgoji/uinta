#ifndef SRC_ENGINE_INCLUDE_UINTA_ENGINE_CURSOR_MANAGER_H_
#define SRC_ENGINE_INCLUDE_UINTA_ENGINE_CURSOR_MANAGER_H_

#include "glm/ext/matrix_float4x4.hpp"
#include "glm/ext/vector_float3.hpp"
#include "glm/ext/vector_uint2.hpp"
#include "uinta/camera/camera_events.h"
#include "uinta/camera/camera_manager.h"
#include "uinta/component.h"
#include "uinta/engine/engine.h"
#include "uinta/engine/engine_state.h"
#include "uinta/math/spatial.h"
#include "uinta/window.h"

namespace uinta {

class CursorManager final : public NewFrameComponent {
 public:
  CursorManager(Engine* engine, CameraManager* camera) noexcept
      : camera_(camera->camera()),
        window_(engine->platform()->window()),
        state_(&engine->state()) {
    camera->addListener<CameraEvent::ViewMatrixUpdated>(
        [this](const auto&) { updateCursorWorldPoint(); });
    camera->addListener<CameraEvent::ProjectionMatrixUpdated>(
        [this](const auto& event) {
          projection_ = event.projection;
          updateCursorWorldPoint();
        });
  }

  void updateCursorWorldPoint() noexcept {
    glm::vec2 viewport(window_->width(), window_->height());
    glm::vec3 ndc((2 * cursorPos_.x) / viewport.x - 1,
                  1 - (2 * cursorPos_.y) / viewport.y, 1);
    auto view = camera_->viewMatrix();
    state_->input().cursorWorldPoint(getWorldPoint(
        viewport, cursorPos_, camera_->position(), view, projection_));
  }

  void update(const EngineState& state) noexcept override {
    if (state.input().cursordx() || state.input().cursordy()) {
      cursorPos_ = {state.input().cursorx(), state.input().cursory()};
      updateCursorWorldPoint();
    }
  }

  glm::uvec2 cursorPos_;
  glm::mat4 projection_;

  const Camera* camera_;
  const Window* window_;
  EngineState* state_;
};

}  // namespace uinta

#endif  // SRC_ENGINE_INCLUDE_UINTA_ENGINE_CURSOR_MANAGER_H_
