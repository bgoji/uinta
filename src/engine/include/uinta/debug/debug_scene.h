#ifndef SRC_ENGINE_INCLUDE_UINTA_DEBUG_DEBUG_SCENE_H_
#define SRC_ENGINE_INCLUDE_UINTA_DEBUG_DEBUG_SCENE_H_

#include "uinta/scene/scene.h"
#include "uinta/scenes/grid.h"

namespace uinta {

struct DebugSceneParams final {
  GridSceneParams grid;
};

class DebugScene : public Scene {
 public:
  explicit DebugScene(Scene*, DebugSceneParams = {}) noexcept;
};

}  // namespace uinta

#endif  // SRC_ENGINE_INCLUDE_UINTA_DEBUG_DEBUG_SCENE_H_
