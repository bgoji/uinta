#ifndef SRC_ENGINE_INCLUDE_UINTA_DEBUG_UI_PRIMITIVE_SHADER_UI_H_
#define SRC_ENGINE_INCLUDE_UINTA_DEBUG_UI_PRIMITIVE_SHADER_UI_H_

#include <imgui.h>

#include "uinta/debug/ui/utils.h"
#include "uinta/gl.h"
#include "uinta/shaders/primitive.h"

namespace uinta {

struct Flags final {
  using value_type = u32;

  static constexpr value_type Normals = 1 << 0;
  static constexpr value_type Polygons = 1 << 1;

  bool isNormals() const noexcept { return flags_ & Normals; }
  void isNormals(bool v) noexcept {
    flags_ &= ~Normals;
    if (v) flags_ |= Normals;
  }

  bool isPolygons() const noexcept { return flags_ & Polygons; }
  void isPolygons(bool v) noexcept {
    flags_ &= ~Polygons;
    if (v) flags_ |= Polygons;
  }

  value_type flags_;
} static flags;

inline void RenderPrimitiveShaderUi(PrimitiveShaderManager* shaderManager,
                                    const OpenGLApi* gl) noexcept {
  if (!ImGui::TreeNode("Primitive shader")) return;

  auto shader = shaderManager->shader();

  ImGui::Text("Render count: %lu", shader->renderCount());

  if (ImGui::CheckboxFlags("Normals", &flags.flags_, Flags::Normals)) {
    ShaderGuard sg(shader);
    shader->normals(flags.isNormals());
  }

  ImGui::SameLine();

  if (ImGui::CheckboxFlags("Polygons", &flags.flags_, Flags::Polygons))
    gl->polygonMode(GL_FRONT_AND_BACK, flags.isPolygons() ? GL_LINE : GL_FILL);

  ImGui::SeparatorText("Lighting");

  auto color = shader->lightColor.value();
  std::array<f32, 3> colorArr = {color.r, color.g, color.b};
  if (ImGui::ColorEdit3("Ambient Color##PrimitiveShaderAmbientColor",
                        colorArr.data())) {
    ShaderGuard guard(shader);
    shader->lightColor = glm::vec3(colorArr[0], colorArr[1], colorArr[2]);
  }

  ImGui::PushItemWidth(UiHalfWidth - UiSpacing * 0.5);
  auto diffuseMin = shader->diffuseMin.value();
  if (ImGui::SliderFloat("##PrimitiveShaderDiffuseMin", &diffuseMin, 0, 1,
                         "Diffuse Min %.2f")) {
    ShaderGuard guard(shader);
    shader->diffuseMin = diffuseMin;
  }

  ImGui::SameLine();

  auto ambientStr = shader->ambientStr.value();
  if (ImGui::SliderFloat("##PrimitiveShaderAmbientStr", &ambientStr, 0, 1,
                         "Ambient Str %.2f")) {
    ShaderGuard guard(shader);
    shader->ambientStr = ambientStr;
  }
  ImGui::PopItemWidth();

  ImGui::PushItemWidth(UiHalfWidth - UiSpacing * 0.5);
  auto lightDir = shader->lightDir.value();
  auto azimuth = std::atan2(lightDir.y, lightDir.x);
  auto elevation = std::asin(lightDir.z);
  bool updateLightDir = false;
  updateLightDir =
      ImGui::SliderAngle("##PrimitiveShaderLightDirAzimuth", &azimuth, -180.0f,
                         180.0f, "Azimuth %.2f") ||
      updateLightDir;

  ImGui::SameLine();

  updateLightDir =
      ImGui::SliderAngle("##PrimitiveShaderLightDirElevation", &elevation,
                         -90.0f, 90.0f, "Elevation %.2f") ||
      updateLightDir;
  ImGui::PopItemWidth();

  if (updateLightDir) {
    ShaderGuard guard(shader);
    shader->lightDir = glm::vec3(cos(elevation) * cos(azimuth), sin(elevation),
                                 cos(elevation) * sin(azimuth));
  }

  ImGui::TreePop();
}

}  // namespace uinta

#endif  // SRC_ENGINE_INCLUDE_UINTA_DEBUG_UI_PRIMITIVE_SHADER_UI_H_
