#include "uinta/debug/debug_scene_ui.h"

#include <imgui.h>
#include <imgui_impl_glfw.h>
#include <imgui_impl_opengl3.h>

#include <string>

#include "uinta/component.h"
#include "uinta/debug/ui/camera_ui.h"
#include "uinta/debug/ui/engine_ui.h"
#include "uinta/debug/ui/imgui_ui.h"
#include "uinta/debug/ui/input_ui.h"
#include "uinta/debug/ui/platform_ui.h"
#include "uinta/debug/ui/primitive_shader_ui.h"
#include "uinta/debug/ui/scene_ui.h"
#include "uinta/engine/engine.h"
#include "uinta/platform.h"
#include "uinta/shaders/primitive.h"

namespace uinta {

f32 UiSpacing = 0;
f32 UiWidth = 300;
f32 UiHalfWidth = UiWidth / 2.0;
f32 UiOneThirdsWidth = UiWidth * 1.0 / 3.0;
f32 UiTwoThirdsWidth = UiWidth * 2.0 / 3.0;
f32 UiOneQuartersWidth = UiWidth * 1.0 / 4.0;
f32 UiOneFifthsWidth = UiWidth * 1.0 / 5.0;
f32 UiThreeQuartersWidth = UiWidth * 3.0 / 4.0;

static EngineUiInfo engineUiInfo_;
static PrimitiveShaderManager* shader_;
static CameraManager* camera_;

DebugSceneUi::DebugSceneUi(Scene* parent) noexcept
    : Scene(parent, "DebugSceneUi", SceneLayer::Debug) {
  shader_ = parent->findSystem<PrimitiveShaderManager>().value_or(nullptr);
  assert(shader_ && "`PrimitiveShaderManager` not found.");

  camera_ = parent->findSystem<CameraManager>().value_or(nullptr);
  assert(camera_ && "`CameraManager` not found.");

  engineUiInfo_.engine = engine();
  engine()->dispatchers()->addListener<EngineEvent::RenderComplete>(
      [](const auto& event) {
        engineUiInfo_.render.push(event.duration);
        engineUiInfo_.frameDelta.push(event.state->frameDelta());
      });

  engine()->dispatchers()->addListener<EngineEvent::TickComplete>(
      [](const auto& event) {
        engineUiInfo_.tick.push(event.duration);
        engineUiInfo_.delta.push(event.state->delta());
      });

  if (!flags_.isImGuiInitialized()) {
    IMGUI_CHECKVERSION();
    ImGui::CreateContext();
    ImGui::StyleColorsDark();

    auto& io = ImGui::GetIO();
    io.ConfigFlags |= ImGuiConfigFlags_NavEnableKeyboard;

    auto& style = ImGui::GetStyle();
    style.WindowMinSize = ImVec2(
        UiWidth + style.DisplayWindowPadding.x * 2 + style.ScrollbarSize, 400);
    style.WindowRounding = 8;
    style.ScrollbarRounding = 4;

    UiSpacing = style.ItemSpacing.x;
    UiOneThirdsWidth -= UiSpacing * (1.0 / 3.0);
    UiTwoThirdsWidth -= UiSpacing * (2.0 / 3.0);
    UiOneQuartersWidth -= UiSpacing * (1.0 / 4.0);
    UiOneFifthsWidth -= UiSpacing * (1.0 / 5.0);
    UiThreeQuartersWidth -= UiSpacing * (3.0 / 4.0);

    ImGui_ImplGlfw_InitForOpenGL(
        static_cast<GLFWwindow*>(engine()->platform()->window()->userData()),
        true);
    ImGui_ImplOpenGL3_Init();
    flags_.isImGuiInitialized(true);
  }

  class FpsCounter : public NewFrameComponent {
   public:
    FpsCounter(EngineUiInfo::FpsCounterAvg*) noexcept {}

    void update(const EngineState& state) noexcept override {
      counter++;
      auto currentSecond = static_cast<size_t>(state.runtime());
      if (auto casted = static_cast<size_t>(state.runtime());
          currentSecond > prevSecond) {
        prevSecond = casted;
        engineUiInfo_.fps.push(counter);
        counter = 0;
      }
    }

   private:
    size_t counter = 0;
    size_t prevSecond = 0;
  };

  addComponent<FpsCounter>(&engineUiInfo_.fps);
}

void DebugSceneUi::preTick(const EngineState&) noexcept {
  if (flags_.isImGuiDeinitialized()) return;
  auto& io = ImGui::GetIO();
  if (io.WantCaptureMouse) engine()->state().input().resetMouse();
  if (io.WantCaptureKeyboard) engine()->state().input().resetKeyboard();
}

void DebugSceneUi::render(const EngineState& state) noexcept {
  if (flags_.isImGuiNewFrame() && flags_.isImGuiDeinitialized()) return;

  ImGui_ImplOpenGL3_NewFrame();
  ImGui_ImplGlfw_NewFrame();
  ImGui::NewFrame();
  ImGui::Begin(std::string("Menu: " + name()).c_str());

  flags_.isImGuiNewFrame(true);
  flags_.isImGuiRendered(false);

  if (flags_.isImGuiRendered() && flags_.isImGuiDeinitialized()) return;

  static auto updatePos = true;
  if (updatePos) {
    auto size = ImGui::GetWindowSize();
    auto pos = ImGui::GetWindowPos();
    auto window = engine()->platform()->window();
    pos.x = std::min(window->width() - size.x, pos.x);
    pos.x = std::max(0.0f, pos.x);
    pos.y = std::min(window->height() - size.y, pos.y);
    pos.y = std::max(0.0f, pos.y);
    ImGui::SetWindowPos(pos);
  }

  RenderCameraUi(camera_);
  RenderEngineUi(engineUiInfo_);
  RenderImGuiUi();
  RenderInputUi(state, engine()->platform()->window());
  RenderPlatformUi(engine()->platform());
  RenderPrimitiveShaderUi(shader_, engine()->gl());
  RenderSceneUi(engine(), state);

  ImGui::Checkbox("Constrain to window", &updatePos);

  if (flags_.isImGuiDeinitialized()) return;
  ImGui::End();
  ImGui::Render();
  ImGui_ImplOpenGL3_RenderDrawData(ImGui::GetDrawData());
  flags_.isImGuiRendered(true);
  flags_.isImGuiNewFrame(false);
}

DebugSceneUi::~DebugSceneUi() noexcept {
  if (flags_.isImGuiDeinitialized()) return;
  ImGui_ImplOpenGL3_Shutdown();
  ImGui_ImplGlfw_Shutdown();
  ImGui::DestroyContext();
  flags_.isImGuiDeinitialized(true);
}

}  // namespace uinta
