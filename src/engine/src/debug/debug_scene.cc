#include "uinta/debug/debug_scene.h"

#include "uinta/debug/debug_scene_ui.h"
#include "uinta/scenes/axis_viewer.h"
#include "uinta/scenes/grid.h"

namespace uinta {

DebugScene::DebugScene(Scene* parent, DebugSceneParams params) noexcept
    : Scene(parent, "DebugScene", SceneLayer::Simulation) {
  addScene<DebugSceneUi>();
  addScene<AxisViewerScene>();
  addScene<GridScene>(params.grid);
}

}  // namespace uinta
