#include "uinta/camera/camera_manager.h"

#include "uinta/camera/camera_events.h"
#include "uinta/engine/engine_events.h"

namespace uinta {

CameraManager::CameraManager(Camera* camera, EngineDispatchers* engine,
                             Projection projection, f32 orthoSize) noexcept
    : orthoSize_(orthoSize), projection_(projection), camera_(camera) {
  engine->addListener<EngineEvent::ViewportSizeChange>(
      [this](const auto& event) {
        glm::mat4 projection;
        aspect_ = event.aspect();
        if (projection_ == Projection::Perspective) {
          projection = camera_->perspectiveMatrix(aspect_);
        } else {
          projection = camera_->orthographicMatrix(orthoSize_, aspect_);
        }
        dispatchers_.dispatch<CameraEvent::ProjectionMatrixUpdated>(
            ProjectionMatrixUpdateEvent(camera_, projection, aspect_));
      });
}

}  // namespace uinta
