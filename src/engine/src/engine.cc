#include "uinta/engine/engine.h"

#include <algorithm>
#include <cassert>

#include "absl/log/log.h"
#include "absl/strings/str_cat.h"
#include "absl/strings/str_format.h"
#include "uinta/component.h"
#include "uinta/file.h"
#include "uinta/scene/scene_events.h"
#include "uinta/system.h"

namespace uinta {

Engine::Engine(Platform* platform, const OpenGLApi* gl,
               FileSystem* fileSystem) noexcept
    : components_(this),
      frame_(platform->primaryMonitor().value_or(nullptr)),
      fileSystem_(fileSystem),
      gl_(gl),
      platform_(platform) {
  assert(platform_ && "`Platform*` cannot be null.");

  platform_->addListener<PlatformEvent::OnCloseRequest>(
      [this](const auto&) { state_.isClosing(true); });

  platform_->addListener<PlatformEvent::OnError>([this](const auto& event) {
    status_ =
        InternalError(absl::StrFormat("%i: %s", event.code, event.description));
  });

  platform_->addListener<PlatformEvent::OnDebugMessage>([](const auto& event) {
    std::string message(event.message, event.length);
    message = absl::StrCat(message, "\n\tID: ", event.id);
    message = absl::StrCat(message, "\n\tSeverity: ",
                           OpenGLApi::GetSeverityString(event.severity));
    message = absl::StrCat(message, "\n\tSource: ", event.source, "\t",
                           OpenGLApi::GetSourceString(event.source));
    message = absl::StrCat(message, "\n\tType: ", event.type, "\t",
                           OpenGLApi::GetTypeString(event.type));

    switch (event.severity) {
      case GL_DEBUG_SEVERITY_NOTIFICATION:
        LOG(INFO) << message;
        break;
      case GL_DEBUG_SEVERITY_LOW:
        LOG(WARNING) << message;
        break;
      case GL_DEBUG_SEVERITY_MEDIUM:
        LOG(ERROR) << message;
        break;
      default:
        LOG(FATAL) << message;
        break;
    }
  });

  platform_->addListener<PlatformEvent::OnViewportSizeChange>(
      [this](const auto&) {
        auto width = platform_->window()->width();
        auto height = platform_->window()->height();
        gl_->viewport(0, 0, width, height);
        dispatchers_.dispatch<EngineEvent::ViewportSizeChange>(
            ViewportSizeChange(width, height));
        LOG(INFO) << absl::StrFormat("Event: Viewport size change (%u, %u)",
                                     width, height);
      });

  gl_->clearColor(1.0, 1.0, 1.0, 1.0);

  platform_->addListener<PlatformEvent::OnMonitorChange>(
      [this](const auto& event) { frame_ = FrameManager(event.monitor); });

  if (status_ = platform_->registerInputHandlers(&state_.input());
      !status_.ok()) {
    LOG(ERROR) << status_.message();
    return;
  }

  state_.update(getRuntime(), 0);
}

void Engine::run() noexcept {
  state_.update(getRuntime(), 0);

  Scene* scene = nullptr;
  time_t timer;
  time_t runtime;

  while (!state_.isClosing() && status_.ok()) {
    if (!scene) {
      scene = prepareNextScene();
    }

    if (scene) {
      scene->removeStaleScenes();
      if (!scene->isRunning()) {
        if (scene && scene->isError()) {
          LOG(ERROR) << absl::StrFormat("`%s` produced an error.",
                                        scene->name());
          status_ = CancelledError();
          break;
        }
        sceneQueue_.pop();
        scene = nullptr;
      }
    }

    if (status_ = platform_->pollEvents(); !status_.ok()) {
      LOG(ERROR) << "`Platform::pollEvents()` failed: " << status_.message();
      break;
    }

    do {
      runtime = getRuntime();
      timer = runtime;
      state_.isNewFrame(runtime >= frame_.nextFrame);
      state_.update(runtime, 1);

      advance<EngineStage::PreTick>(scene);
      advance<EngineStage::Tick>(scene);
      advance<EngineStage::PostTick>(scene);

      runtime = getRuntime();
      dispatchers_.dispatch<EngineEvent::TickComplete>(
          TickComplete(&state_, runtime - timer));
    } while (!state_.isNewFrame());

    components_.update<ComponentType::NewFrame>(state_);
    systems_.update<SystemType::NewFrame>(state_);

    if (scene) {
      scene->updateComponents<ComponentType::NewFrame>(state_);
      scene->updateSystems<SystemType::NewFrame>(state_);
    }

    runtime = getRuntime();
    timer = runtime;
    frame_.nextFrame =
        runtime + (state_.isFixedTickRate() ? 0 : frame_.nextFrameAdvance);

    advance<EngineStage::PreRender>(scene);

    gl_->clear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    advance<EngineStage::Render>(scene);
    if (status_ = platform_->swapBuffers(); !status_.ok())
      status_ = InternalError("`Platform::swapBuffers()` failed");

    advance<EngineStage::PostRender>(scene);

    runtime = getRuntime();
    dispatchers_.dispatch<EngineEvent::RenderComplete>(
        RenderComplete(&state_, runtime - timer));

    state_.input().reset();
  }
}

void Engine::updateRenderOrder() noexcept {
  struct Comparator final {
    bool operator()(const Scene* a, const Scene* b) const noexcept {
      return a->layer() > b->layer();
    }
  };

  std::priority_queue<Scene*, std::vector<Scene*>, Comparator> queue;

  if (!sceneQueue_.empty()) {
    auto* front = sceneQueue_.front().get();
    queue.push(front);
    std::for_each(front->children().begin(), front->children().end(),
                  [&queue](auto& scene) {
                    queue.push(scene.get());
                    std::for_each(
                        scene->children().begin(), scene->children().end(),
                        [&queue](auto& child) { queue.push(child.get()); });
                  });
  }

  renderOrder_.clear();
  renderOrder_.reserve(queue.size());

  while (!queue.empty()) {
    renderOrder_.push_back(queue.top());
    queue.pop();
  }

  LOG(INFO) << "Render order updated.";
}

Scene* Engine::prepareNextScene() noexcept {
  static std::function<void(std::unique_ptr<Scene>&)> registerListeners =
      [&](auto& scene) {
        scene->dispatchers()->template addListener<SceneEvent::SceneAdded>(
            [this](const auto&) { updateRenderOrder(); });
        scene->dispatchers()->template addListener<SceneEvent::SceneRemoved>(
            [this](const auto&) { updateRenderOrder(); });
        scene->dispatchers()->template addListener<SceneEvent::LayerChange>(
            [this](const auto&) { updateRenderOrder(); });

        std::for_each(scene->children().begin(), scene->children().end(),
                      registerListeners);
      };

  if (sceneQueue_.empty()) return nullptr;

  registerListeners(sceneQueue_.front());
  updateRenderOrder();

  return sceneQueue_.front().get();
}

void Engine::preTick(Scene* scene) noexcept {
  if (scene && scene->isTicking()) {
    scene->preTick(state_);
    std::for_each(scene->children().begin(), scene->children().end(),
                  [this](auto& scene) { preTick(scene.get()); });
  }
}

void Engine::tick(Scene* scene) noexcept {
  if (scene && scene->isTicking()) {
    scene->tick(state_);
    std::for_each(scene->children().begin(), scene->children().end(),
                  [this](auto& scene) { tick(scene.get()); });
  }
}

void Engine::postTick(Scene* scene) noexcept {
  if (scene && scene->isTicking()) {
    scene->postTick(state_);
    std::for_each(scene->children().begin(), scene->children().end(),
                  [this](auto& scene) { postTick(scene.get()); });
  }
}

void Engine::preRender(Scene* scene) noexcept {
  if (scene && scene->isRendering()) {
    scene->preRender(state_);
    std::for_each(scene->children().begin(), scene->children().end(),
                  [this](auto& scene) { preRender(scene.get()); });
  }
}

void Engine::render(Scene* scene) noexcept {
  if (scene && scene->isRendering()) {
    scene->render(state_);
  }
}

void Engine::postRender(Scene* scene) noexcept {
  if (scene && scene->isRendering()) {
    scene->postRender(state_);
    std::for_each(scene->children().begin(), scene->children().end(),
                  [this](auto& scene) { postRender(scene.get()); });
  }
}

}  // namespace uinta
