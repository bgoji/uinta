#ifndef SRC_ENGINE_TEST_SRC_UTILS_H_
#define SRC_ENGINE_TEST_SRC_UTILS_H_

#include <gtest/gtest.h>

#include "./mock/mock_file_system.h"
#include "./mock/mock_gl.h"
#include "./mock/mock_platform.h"
#include "absl/log/log.h"
#include "absl/synchronization/mutex.h"
#include "absl/time/time.h"
#include "uinta/engine/engine.h"

namespace uinta {

inline void runThenClose(
    Engine* engine, std::function<bool()> condition = [] { return true; },
    absl::Duration timeout = absl::Microseconds(20),
    absl::Duration forceTimeout = absl::Seconds(1)) noexcept {
  absl::Mutex mtx;
  auto threadFinished = false;

  std::thread thread([&]() {
    engine->run();
    {
      absl::MutexLock lock(&mtx);
      threadFinished = true;
    }
  });

  {
    absl::MutexLock lock(&mtx);
    auto conditionMet =
        mtx.AwaitWithTimeout(absl::Condition(&condition), timeout);

    engine->state().isClosing(true);

    if (!conditionMet) {
      LOG(ERROR) << "Condition was not met within the timeout period.";

      if (!mtx.AwaitWithTimeout(absl::Condition(&threadFinished), forceTimeout))
        LOG(ERROR) << "Engine did not exit after force timeout, potential hang "
                      "detected.";
    }
  }

  if (thread.joinable()) thread.join();
}

class UintaTestF : public ::testing::Test {
 protected:
  MockPlatform platform;
  MockOpenGLApi gl;
  MockFileSystem fileSystem;

  Engine makeEngine() noexcept { return Engine(&platform, &gl, &fileSystem); }
};

}  // namespace uinta

#endif  // SRC_ENGINE_TEST_SRC_UTILS_H_
