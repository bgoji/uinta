#ifndef SRC_ENGINE_TEST_SRC_MOCK_MOCK_FILE_SYSTEM_H_
#define SRC_ENGINE_TEST_SRC_MOCK_MOCK_FILE_SYSTEM_H_

#include <functional>
#include <string>
#include <vector>

#include "uinta/file.h"
#include "uinta/status.h"

namespace uinta {

struct MockFileSystem : FileSystem {
  MockFileSystem() noexcept : FileSystem() {}

  std::function<StatusOr<bool>(const std::string&)> onExists = [](const auto&) {
    return true;
  };
  StatusOr<bool> exists(const std::string& path) const noexcept {
    return onExists(path);
  }

  std::function<void()> onClose = []() {};
  void close() noexcept { return onClose(); }

  std::function<Status(const std::string&, std::ios::openmode)> onOpen =
      [](const auto&, auto) { return OkStatus(); };
  Status open(const std::string& path, std::ios::openmode mode) noexcept {
    return onOpen(path, mode);
  }

  std::function<bool()> onIsOpen = []() { return false; };
  bool isOpen() const noexcept { return onIsOpen(); }

  std::function<std::optional<std::string>()> onReadLine = []() { return ""; };
  std::optional<std::string> readLine() noexcept { return onReadLine(); }

  std::function<std::vector<std::string>()> onReadAllLines = []() {
    return std::vector<std::string>();
  };
  std::vector<std::string> readAllLines() noexcept { return onReadAllLines(); }

  std::function<std::string()> onReadAll = []() { return ""; };
  std::string readAll() noexcept { return onReadAll(); }

  std::function<std::vector<u8>()> onReadBinary = []() {
    return std::vector<u8>();
  };
  std::vector<u8> readBinary() noexcept { return onReadBinary(); }
};

}  // namespace uinta

#endif  // SRC_ENGINE_TEST_SRC_MOCK_MOCK_FILE_SYSTEM_H_
