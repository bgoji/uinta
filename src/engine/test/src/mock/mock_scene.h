#ifndef SRC_ENGINE_TEST_SRC_MOCK_MOCK_SCENE_H_
#define SRC_ENGINE_TEST_SRC_MOCK_MOCK_SCENE_H_

#include <string>

#include "uinta/engine/engine_state.h"
#include "uinta/scene/scene.h"

namespace uinta {

struct MockScene : Scene {
  static size_t TestSceneId;
  explicit MockScene(Engine* engine,
                     std::string name = absl::StrFormat("Scene%i",
                                                        TestSceneId++),
                     SceneLayer layer = SceneLayer::Simulation) noexcept
      : Scene(engine, name, layer) {}

  explicit MockScene(Scene* parent,
                     std::string name = absl::StrFormat("Scene%i",
                                                        TestSceneId++),
                     SceneLayer layer = SceneLayer::Simulation) noexcept
      : Scene(parent, name, layer) {}

  std::function<void(const EngineState&)> onPreTick = [](const auto&) {};
  void preTick(const EngineState& state) noexcept { onPreTick(state); }

  std::function<void(const EngineState&)> onTick = [](const auto&) {};
  void tick(const EngineState& state) noexcept { onTick(state); }

  std::function<void(const EngineState&)> onPostTick = [](const auto&) {};
  void postTick(const EngineState& state) noexcept { onPostTick(state); }

  std::function<void(const EngineState&)> onPreRender = [](const auto&) {};
  void preRender(const EngineState& state) noexcept { onPreRender(state); }

  std::function<void(const EngineState&)> onRender = [](const auto&) {};
  void render(const EngineState& state) noexcept { onRender(state); }

  std::function<void(const EngineState&)> onPostRender = [](const auto&) {};
  void postRender(const EngineState& state) noexcept { onPostRender(state); }

  virtual void onDebugUi(const EngineState&) noexcept {}
};

}  // namespace uinta

#endif  // SRC_ENGINE_TEST_SRC_MOCK_MOCK_SCENE_H_
