#include "uinta/engine/engine.h"

#include <gtest/gtest.h>

#include "./mock/mock_component.h"
#include "./utils.h"

namespace uinta {

class EngineTest : public UintaTestF {};

TEST_F(EngineTest, ConstructorStartTimeUpdated) {
  bool runtimeCalled = false;
  platform.onRuntime = [&]() {
    runtimeCalled = true;
    return platform.runtimeGetter_.runtime();
  };

  auto engine = makeEngine();

  ASSERT_TRUE(runtimeCalled) << "`Engine()` was expected to capture the "
                                "current runtime of the application.";

  ASSERT_DOUBLE_EQ(MockRuntimeGetter::RuntimeStep, engine.state().runtime())
      << "`EngineState::runtime()` was expected to be updated in `Engine()`";
}

TEST_F(EngineTest, ConstructorOnClosingListening) {
  auto engine = makeEngine();
  ASSERT_FALSE(engine.state().isClosing());
  platform.dispatch<PlatformEvent::OnCloseRequest>(
      OnCloseRequestEvent(nullptr));
  ASSERT_TRUE(engine.state().isClosing());
}

TEST_F(EngineTest, ConstructorOnErrorListening) {
  auto engine = makeEngine();
  ASSERT_TRUE(engine.status().ok());
  platform.dispatch<PlatformEvent::OnError>(OnErrorEvent(0, ""));
  ASSERT_TRUE(IsInternal(engine.status()));
}

TEST_F(EngineTest, ConstructorOnViewportSizeChangeListening) {
  auto engine = makeEngine();

  bool eventCalled = false;
  engine.dispatchers()->addListener<EngineEvent::ViewportSizeChange>(
      [&eventCalled](const auto&) { eventCalled = true; });

  bool onViewportCalled = false;
  gl.onViewport = [&onViewportCalled](auto, auto, auto, auto) {
    onViewportCalled = true;
  };
  platform.dispatch<PlatformEvent::OnViewportSizeChange>(
      OnViewportSizeChangeEvent(nullptr, 0, 0));
  ASSERT_TRUE(onViewportCalled);
  ASSERT_TRUE(eventCalled);
}

TEST_F(EngineTest, ConstructorClearColorSet) {
  bool clearColorCalled = false;
  gl.onClearColor = [&](GLfloat, GLfloat, GLfloat, GLfloat) {
    clearColorCalled = true;
  };

  auto engine = makeEngine();
  runThenClose(&engine);
  ASSERT_TRUE(clearColorCalled);
}

TEST_F(EngineTest, StopOnStateClosing) {
  auto engine = makeEngine();
  engine.state().isClosing(true);
  runThenClose(&engine);
  ASSERT_EQ(0, engine.state().tick());
}

TEST_F(EngineTest, RunRuntimeUpdate) {
  auto engine = makeEngine();
  ASSERT_DOUBLE_EQ(MockRuntimeGetter::RuntimeStep, engine.state().runtime())
      << "Runtime was expected to have advanced in the constructor.";

  engine.state().isClosing(true);
  engine.run();

  constexpr size_t runtimeCallCountExpected = 2;
  ASSERT_DOUBLE_EQ(MockRuntimeGetter::RuntimeStep * runtimeCallCountExpected,
                   engine.state().runtime())
      << "Runtime was expected to advance two steps in `Engine::run()`.";
}

TEST_F(EngineTest, StatusNotOkOnRun) {
  auto engine = makeEngine();
  engine.status() = InternalError("Testing error.");
  ASSERT_EQ(0, engine.state().tick());
  runThenClose(&engine);
}

TEST_F(EngineTest, SingleTickAdvancement) {
  auto engine = makeEngine();
  ASSERT_EQ(0, engine.state().tick());
  runThenClose(&engine, [&]() { return engine.state().tick() > 0; });
  ASSERT_NE(0, engine.state().tick());
}

TEST_F(EngineTest, SwapBuffersCalled) {
  auto engine = makeEngine();
  bool swapBuffersCalled = false;
  auto status = platform.onSwapBuffers = [&swapBuffersCalled]() {
    swapBuffersCalled = true;
    return OkStatus();
  };
  runThenClose(&engine, [&swapBuffersCalled]() { return swapBuffersCalled; });
  ASSERT_TRUE(swapBuffersCalled);
}

TEST_F(EngineTest, ClearCalled) {
  auto engine = makeEngine();
  bool clearCalled = false;
  GLbitfield bitsVal = 0;
  gl.onClear = [&clearCalled, &bitsVal](GLbitfield bits) {
    clearCalled = true;
    bitsVal = bits;
    return OkStatus();
  };
  runThenClose(&engine, [&clearCalled]() { return clearCalled; });
  ASSERT_TRUE(clearCalled);
  EXPECT_TRUE(bitsVal | GL_COLOR_BUFFER_BIT);
  EXPECT_TRUE(bitsVal | GL_DEPTH_BUFFER_BIT);
}

TEST_F(EngineTest, EventTickComplete) {
  auto engine = makeEngine();
  bool tickCompleteEventCalled = false;
  engine.dispatchers()->addListener<EngineEvent::TickComplete>(
      [&tickCompleteEventCalled](const auto&) {
        tickCompleteEventCalled = true;
      });
  runThenClose(&engine, [&tickCompleteEventCalled]() {
    return tickCompleteEventCalled;
  });
  ASSERT_TRUE(tickCompleteEventCalled);
}

TEST_F(EngineTest, EventRenderComplete) {
  auto engine = makeEngine();
  bool renderCompleteEventCalled = false;
  engine.dispatchers()->addListener<EngineEvent::RenderComplete>(
      [&renderCompleteEventCalled](const auto&) {
        renderCompleteEventCalled = true;
      });
  runThenClose(&engine, [&renderCompleteEventCalled]() {
    return renderCompleteEventCalled;
  });
  ASSERT_TRUE(renderCompleteEventCalled);
}

TEST_F(EngineTest, EventOnMonitorChange) {
  auto engine = makeEngine();
  auto primaryMonitor = platform.primaryMonitor();
  ASSERT_NE(std::nullopt, primaryMonitor);
  auto firstMonExpectedAdvance =
      1.0 / static_cast<time_t>(primaryMonitor.value()->hz());
  ASSERT_FLOAT_EQ(firstMonExpectedAdvance,
                  engine.frameManager().nextFrameAdvance);

  Monitor secondMon("Second monitor", 1920, 1080, 144);
  platform.dispatch<PlatformEvent::OnMonitorChange>(
      OnMonitorChangeEvent(&secondMon));
  auto secondMonExpectedAdvance = 1.0 / secondMon.hz();
  ASSERT_FLOAT_EQ(secondMonExpectedAdvance,
                  engine.frameManager().nextFrameAdvance);
}

TEST_F(EngineTest, AddComponent) {
  auto engine = makeEngine();
  auto* component = engine.addComponent<MockComponent<NewFrameComponent>>();
  auto expected = engine.components()->newFrameComponents().end();
  auto itr = std::find_if(
      engine.components()->newFrameComponents().begin(),
      engine.components()->newFrameComponents().end(),
      [component](const auto& ptr) { return ptr.get() == component; });
  ASSERT_NE(expected, itr);
}

TEST_F(EngineTest, RemoveComponent) {
  auto engine = makeEngine();
  auto* component = engine.addComponent<MockComponent<NewFrameComponent>>();
  engine.removeComponent(component);
  auto expected = engine.components()->newFrameComponents().end();
  auto itr = std::find_if(
      engine.components()->newFrameComponents().begin(),
      engine.components()->newFrameComponents().end(),
      [component](const auto& ptr) { return ptr.get() == component; });
  ASSERT_EQ(expected, itr);
}

TEST_F(EngineTest, ComponentNewFrame) {
  auto engine = makeEngine();
  auto* component = engine.addComponent<MockComponent<NewFrameComponent>>();
  ASSERT_FALSE(component->updateCalled);
  ASSERT_EQ(0, engine.state().tick());
  runThenClose(&engine, [&component]() { return component->updateCalled; });
  ASSERT_TRUE(component->updateCalled);
  ASSERT_NE(0, engine.state().tick());
}

TEST_F(EngineTest, ComponentPostRender) {
  auto engine = makeEngine();
  auto* component = engine.addComponent<MockComponent<PostRenderComponent>>();
  ASSERT_FALSE(component->updateCalled);
  runThenClose(&engine, [&component]() { return component->updateCalled; });
  ASSERT_TRUE(component->updateCalled);
}

TEST_F(EngineTest, ComponentPostTick) {
  auto engine = makeEngine();
  auto* component = engine.addComponent<MockComponent<PostTickComponent>>();
  ASSERT_FALSE(component->updateCalled);
  runThenClose(&engine, [&component]() { return component->updateCalled; });
  ASSERT_TRUE(component->updateCalled);
}

TEST_F(EngineTest, ComponentPreRender) {
  auto engine = makeEngine();
  auto* component = engine.addComponent<MockComponent<PreRenderComponent>>();
  ASSERT_FALSE(component->updateCalled);
  runThenClose(&engine, [&component]() { return component->updateCalled; });
  ASSERT_TRUE(component->updateCalled);
}

TEST_F(EngineTest, ComponentPreTick) {
  auto engine = makeEngine();
  auto* component = engine.addComponent<MockComponent<PreTickComponent>>();
  ASSERT_FALSE(component->updateCalled);
  runThenClose(&engine, [&component]() { return component->updateCalled; });
  ASSERT_TRUE(component->updateCalled);
}

TEST_F(EngineTest, ComponentRender) {
  auto engine = makeEngine();
  auto* component = engine.addComponent<MockComponent<RenderComponent>>();
  ASSERT_FALSE(component->updateCalled);
  runThenClose(&engine, [&component]() { return component->updateCalled; });
  ASSERT_TRUE(component->updateCalled);
}

TEST_F(EngineTest, ComponentTick) {
  auto engine = makeEngine();
  auto* component = engine.addComponent<MockComponent<TickComponent>>();
  ASSERT_FALSE(component->updateCalled);
  runThenClose(&engine, [&component]() { return component->updateCalled; });
  ASSERT_TRUE(component->updateCalled);
}

TEST_F(EngineTest, SystemExecuting) {
  class TestSystem : public System {
   public:
    bool onPreTickCalled = false;
    void onPreTick(const EngineState&) noexcept override {
      if (onPreTickCalled) return;
      onPreTickCalled = true;
      ASSERT_EQ(0, callIndex);
      callIndex++;
    }

    bool onTickCalled = false;
    void onTick(const EngineState&) noexcept override {
      if (onTickCalled) return;
      onTickCalled = true;
      ASSERT_EQ(1, callIndex);
      callIndex++;
    }

    bool onPostTickCalled = false;
    void onPostTick(const EngineState&) noexcept override {
      if (onPostTickCalled) return;
      onPostTickCalled = true;
      ASSERT_EQ(2, callIndex);
      callIndex++;
    }

    bool onNewFrameCalled = false;
    void onNewFrame(const EngineState&) noexcept override {
      if (onNewFrameCalled) return;
      onNewFrameCalled = true;
      ASSERT_EQ(3, callIndex);
      callIndex++;
    }

    bool onPreRenderCalled = false;
    void onPreRender(const EngineState&) noexcept override {
      if (onPreRenderCalled) return;
      onPreRenderCalled = true;
      ASSERT_EQ(4, callIndex);
      callIndex++;
    }

    bool onRenderCalled = false;
    void onRender(const EngineState&) noexcept override {
      if (onRenderCalled) return;
      onRenderCalled = true;
      ASSERT_EQ(5, callIndex);
      callIndex++;
    }

    bool onPostRenderCalled = false;
    void onPostRender(const EngineState&) noexcept override {
      if (onPostRenderCalled) return;
      onPostRenderCalled = true;
      ASSERT_EQ(6, callIndex);
      callIndex++;
    }

    size_t callIndex = 0;
  };

  auto engine = makeEngine();
  auto* system = engine.addSystem<TestSystem>();
  runThenClose(&engine, [&system]() { return system->onPostRenderCalled; });

  ASSERT_TRUE(system->onPreTickCalled);
  ASSERT_TRUE(system->onTickCalled);
  ASSERT_TRUE(system->onPostTickCalled);
  ASSERT_TRUE(system->onNewFrameCalled);
  ASSERT_TRUE(system->onPreRenderCalled);
  ASSERT_TRUE(system->onRenderCalled);
  ASSERT_TRUE(system->onPostRenderCalled);
}

}  // namespace uinta
